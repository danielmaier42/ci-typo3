#!/bin/bash

SSH_PRIVATE_KEY=$1

# Install ssh-agent if not already installed, it is required by Docker.
which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )

# Run ssh-agent (inside the build environment)
eval $(ssh-agent -s)

# Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
ssh-add <(echo "$SSH_PRIVATE_KEY")

# Create the SSH directory and give it the right permissions
mkdir -p ~/.ssh
chmod 700 ~/.ssh