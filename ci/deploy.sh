#!/bin/bash

VERSION=`date +%s`

HOST=$1
PORT=$2
USER=$3
COMPOSER=$4
DOCUMENT_ROOT=$5

DATA_ROOT="$DOCUMENT_ROOT/data"
DATA_FILEADMIN="$DATA_ROOT/fileadmin"
DATA_UPLOADS="$DATA_ROOT/uploads"
DATA_CONFIG="$DATA_ROOT/AdditionalConfiguration.php"
DATA_HTACCESS="$DATA_ROOT/.htaccess"

if [ -z "$PORT" ]; then
    PORT="22"
fi

echo "Deploying version $VERSION..."
echo "- Server: $HOST"
echo "- Port: $PORT"
echo "- User: $USER"
echo "- Composer: $COMPOSER"
echo "- Root: $DOCUMENT_ROOT"

echo "Rechecking SSH Identies:"
ssh-add -L

if [ -z "$HOST" ]; then
    >&2 echo "Host must be set!"
    exit -1
fi

if [ -z "$USER" ]; then
    >&2 echo "User must be set!"
    exit -1
fi

if [ -z "$COMPOSER" ]; then
    >&2 echo "Composer path must be defined!"
    exit -1
fi

if [ -z "$DOCUMENT_ROOT" ]; then
    >&2 echo "Document Root must be defined!"
    exit -1
fi

ssh "$USER"@$"$HOST" -p "$PORT" -o StrictHostKeyChecking=no -tt << EOF
    if [ ! -d "$DOCUMENT_ROOT" ]; then
        mkdir -p "$DOCUMENT_ROOT"
    fi

    if [ ! -d "$DOCUMENT_ROOT" ]; then
        >&2 echo "Document Root does not exist and could not create it neither"
        exit -1
    fi

    if [ ! -d "$DATA_ROOT" ]; then
        mkdir -p "$DATA_ROOT"
    fi

    if [ ! -d "$DATA_FILEADMIN" ]; then
        mkdir -p "$DATA_FILEADMIN"
    fi

    if [ ! -d "$DATA_UPLOADS" ]; then
        mkdir -p "$DATA_UPLOADS"
    fi

    if [ ! -f "$DATA_CONFIG" ]; then
        touch "$DATA_CONFIG"
    fi

    cd "$DOCUMENT_ROOT"
    
    mkdir -p "deployment/$VERSION/"

    if [ ! -d "deployment/$VERSION/" ]; then 
        >&2 echo "Could not create Deployment Path for Version $VERSION"
        exit -1 
    fi

    git clone $CI_REPOSITORY_URL "deployment/$VERSION/"

    if [ $? -ne 0 ]; then
        cd "$DOCUMENT_ROOT"
        rm -rf "deployment/$VERSION/"
        >&2 echo "Could not clone Git"
        exit -1
    fi

    cd "deployment/$VERSION/"
    git checkout $CI_COMMIT_REF_NAME

    if [ $? -ne 0 ]; then
        cd "$DOCUMENT_ROOT"
        rm -rf "deployment/$VERSION/"
        >&2 echo "Could checkout $CI_COMMIT_REF_NAME"
        exit -1
    fi

    cd "app"

    eval "$COMPOSER install"

    if [ ! -d "vendor" ]; then
        cd "$DOCUMENT_ROOT"
        rm -rf "deployment/$VERSION/"
        >&2 echo "Cannot install Composer dependencies"
        exit -1
    fi

    cd "web"

    ln -s "$DATA_FILEADMIN" "fileadmin"
    ln -s "$DATA_UPLOADS" "uploads"
    ln -s "$DATA_CONFIG" "typo3conf/AdditionalConfiguration.php"
    ln -s "$DATA_HTACCESS" ".htaccess"

    cd "$DOCUMENT_ROOT"

    if [ -D "active" ]; then
        mkdir -p "archives"
        mv "active" "archives/$VERSION"
    fi

    if [ -L "active" ]; then
        rm "active"
    fi

    ln -s "deployment/$VERSION/" "active"

    exit
EOF

exit $?